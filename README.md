## Span package for thirdorder Python script

Snap package of https://bitbucket.org/sousaw/thirdorder/src/master/

You can install thirdoder as a snap by running:

```
sudo snap install thirdorder
```

Interfaces to CASTEP, VASP, QUANTUM ESPRESSO and common are called by running:

```thirdorder.castep```

```thirdorder.vasp```

```thirdorder.espresso```

```thirdorder.common```

For more information on how to use the program, please refer to the original repository.